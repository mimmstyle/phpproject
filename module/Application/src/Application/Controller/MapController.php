<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Db\Adapter\Adapter;
use Zend\Mail\Transport\Smtp;
use Zend\Mail;

use Zend\Cache\StorageFactory;
use Zend\Cache\Storage\Adapter\Memcached;
use Zend\Cache\Storage\StorageInterface;
use Zend\Cache\Storage\AvailableSpaceCapableInterface;
use Zend\Cache\Storage\FlushableInterface;
use Zend\Cache\Storage\TotalSpaceCapableInterface;

/**
* Class MapController
*/
class MapController extends AbstractActionController{
################################################################################
	public function __construct(){
		$this->cacheTime = 36000;
        $this->now = date("Y-m-d H:i:s");
        $this->config = include __DIR__ . '../../../../config/module.config.php';
        $this->adapter = new Adapter($this->config['Db']);
	}
################################################################################
	private function basic(){
		$view = new ViewModel();
		//Route
		$view->lang = $this->param()->fromRoute('lang','th');
		$view->action = $this->param()->fromRoute('action', 'index');
		$view->id = $this->param()->fromRoute('id','');
		$view->page = $this->param()->fromQuery('page', 1);
		
		return null;
	}
################################################################################
	public function indexAction(){
		try{
			//echo "Hello MemberController";
			//return $view;

			$act = $this->params()->fromQuery('act', '');
			if($act == 'sendMail'){

				$message = "
				<html>
				<body>
					<p>Dear Line Manager</p><br/>
					<p>As being a candidate for applying a job, I got an assignment from Mr. Somkiat to do as following:</p>
				  	<p>1. Create new controller.</p>
				   	<p>2. Use Google API for feeding some data, and display on Google map with custom Google map pin.</p>
				    <p>3. And after click on the pin, let show detail of that place and click some button to send email to Line Manager (Tonywilk@scg.com) with GIT url of code.</p>
					<br/>
					<p>So, this email has been sent in order to submit the finished assignment. Please see the source code from the url => https://bitbucket.org/mimmstyle/phpproject/src</p>
					<br/>
					<p>Thank you for your kind consideration.</p>
					<br/>
					<p>Best Regards,</p>
					<p>Parichat Kiatphao</p>
				</body>
				</html>";

				// Setup SMTP Message
				$email = new \Zend\Mail\Message();
				$email->setFrom("kiatphao.p@gmail.com", "Parichat Kiatphao");
				$email->addTo("Tonywilk@scg.com");
				$email->setSubject("SCG - Submit an assignment for job application");

				$bodyPart = new \Zend\Mime\Message();
				$bodyMessage = new \Zend\Mime\Part($message);
    			$bodyMessage->type = 'text/html';

    			$bodyPart->setParts(array($bodyMessage));

    			$email->setBody($bodyPart);
    			$email->setEncoding('UTF-8');

				// Setup SMTP transport
				$option = new \Zend\Mail\Transport\SmtpOptions(
					array(
						'name' 	=> 	"Gmail.com" ,
						'host'	=>	"smtp.gmail.com",
						'port'	=>	587,
						'connection_class'	=> "login",
						'connection_config'	=>  array(
							'username'	=>	'parichat.developer@gmail.com', //For testing purpose
							'password'	=>	'qazxsw21234', //For testing purpose
							'ssl'		=>	'tls'
							)
						)
					);

				$smtp = new \Zend\Mail\Transport\Smtp($option);
				$smtp->send($email);
			}
		}catch(Exception $e){
			print_r($e);
		}
	}
}